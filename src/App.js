import './App.css';
import { BrowserRouter } from 'react-router-dom';
import Header from './components/Header';
import NavRoute from './components/NavRoute';
import Footer from './components/Footer';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <main>
        <NavRoute />
      </main>
      <Footer />
    </BrowserRouter>
  );
}

export default App;
