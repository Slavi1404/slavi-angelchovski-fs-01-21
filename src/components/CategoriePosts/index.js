import React from "react";
import newPost from "../../models/PostModel";
import SinglePost from "../SinglePost";
import { withRouter } from "react-router";

class CategoriePosts extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      article: [],
      categorie: '',
    }
  }

  async componentDidMount(){
    const res = await fetch( `http://progetto-finale.test/web/wp-json/wp/v2/posts?categories=${this.props.match.params.id}` );
    const posts = await res.json();

    const modPosts = await posts.map( post => newPost(post) );
    const categorieName = this.props.match.params.name;

    this.setState({
      article : modPosts,
      categorie: categorieName
    });
  }

  async componentDidUpdate(){
    if( this.props.match.params.name !== this.state.categorie ){
      const res = await fetch( `http://progetto-finale.test/web/wp-json/wp/v2/posts?categories=${this.props.match.params.id}` );
      const posts = await res.json();

      const modPosts = await posts.map( post => newPost(post) );
      const categorieName = this.props.match.params.name;

      this.setState({
        article : modPosts,
        categorie: categorieName
      });
    }
  }

  render(){
    const post = this.state.article.map(post => <SinglePost key={ post.id } post={ post } />);

    return (
      <div className="container my-5">
        <h1>
          { this.state.categorie ? `Categoria post: ${this.state.categorie}` : '' }
        </h1>
        <div className='cantainer row justify-content-center g-0'>
          { post }
        </div>
      </div> 
    );
  }
}

export default withRouter(CategoriePosts);