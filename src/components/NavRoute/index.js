import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../Home";
import NotFound from "../NotFound";
import CategoriePosts from "../CategoriePosts";
import PostFullPage from "../PostFullPage";
import DinamicPage from "../DinamcPage";

class NavRoute extends React.Component{
  render(){
    return(
      <Switch>
        <Route path="/" exact>
          <Home />
        </Route>
        <Route path="/page/:id" exact>
          <DinamicPage />
        </Route>
        <Route path="/categorie/:id/:name" exact>
          <CategoriePosts />
        </Route>
        <Route path="/post/:id" exact>
          <PostFullPage />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    );
  }
}

export default NavRoute;