import React from "react";

class NotFound extends React.Component{
  render(){
    return(
      <div className="alert alert-danger" role="alert">
        Errore - 404 Pagina non trovata!!
      </div>
    );
  }
}

export default NotFound;