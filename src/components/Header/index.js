import React from "react";
import logoReact from '../../image/logo.svg';
import logoWp from '../../image/logo-wp.svg';
import Navbar from "../Navbar";

class Header extends React.Component{
  render(){
    return(
      <header>
        <div className="logos bg-dark">
          <img src={logoReact} className="App-logo" alt="logo" />
          <img src={logoWp} className="logo-wp" alt="logo" />
        </div>
        <div className="navbar-link">
          <Navbar />
        </div>
      </header>
    );
  }
}

export default Header;