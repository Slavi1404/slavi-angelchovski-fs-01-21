import React from "react";
import { Link } from "react-router-dom";

class SinglePost extends React.Component{
  render(){
    return(
      <div className="card col-auto m-5 shadow-lg rounded" style={{width: '18rem'}}>
        <img src={ this.props.post.img } className="card-img-top" alt="immagine" />
        <div className="card-body">
          <Link to={ `/post/${this.props.post.id}` }>
            <h3 className="card-title">{ this.props.post.title }</h3>
          </Link>
          <p className="card-text" dangerouslySetInnerHTML={{ __html: this.props.post.excerpt }}></p>
          <Link to={ `/post/${this.props.post.id}` } className="link-post">... contunia a leggere</Link>
        </div>
      </div>
    );
  }
}

export default SinglePost;