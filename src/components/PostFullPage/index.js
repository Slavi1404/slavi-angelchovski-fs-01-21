import React from "react";
import { withRouter } from "react-router";
import newPostFullPage from "../../models/PostFullPageModel";

class PostFullPage extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      post: {
        title: '',
        img: '',
        content: '',
      }
    }
  }

  async componentDidMount(){
    const res = await fetch(`http://progetto-finale.test/web/wp-json/wp/v2/posts/${this.props.match.params.id}`);
    const post = await res.json();

    const newPost = newPostFullPage(post)

    this.setState({
      post: newPost
    });
    console.log(this.state);
  }


  render(){
    return(
      <div className="container my-5 text-center">
        <h1>{this.state.post.title}</h1>
        <img src={ this.state.post.img } className="img-post img-thumbnail my-3" alt="immagine post" />
        <div dangerouslySetInnerHTML={{__html: this.state.post.content}}></div>
      </div>
    );
  }
}

export default withRouter(PostFullPage);