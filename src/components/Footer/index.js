import React from "react";

class Footer extends React.Component{
  render(){
    return(
      <footer className="p-3 bg-dark text-white text-center">
        <p>La mia prima pagina con React e WordPress</p>
        <p>P.IVA IT1635975356</p>
        <p>By Slavi Angelchovski || socio unico</p>
      </footer>
    );
  }
}

export default Footer;