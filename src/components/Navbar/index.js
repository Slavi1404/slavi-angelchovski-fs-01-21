import React from "react";
import { Link } from "react-router-dom";

class Navbar extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      pages: [],
      categories: []
    }
  }

  async componentDidMount() {
    const categoriesMod = [];
    const pagesMod = [];

    const res = await fetch('http://progetto-finale.test/web/wp-json/wp/v2/categories');
    const categories = await res.json();

    categories.map(async categorie => {
      let obj = {
        id: categorie.id,
        name: categorie.name,
        slug: categorie.slug
      }

      if(categorie.name !== 'null'){
        categoriesMod.push(obj);
      }
    });

    const res2 = await fetch('http://progetto-finale.test/web/wp-json/wp/v2/pages');
    const pages = await res2.json();

    pages.map(async page => {
      let obj = {
        id: page.id,
        name: page.title.rendered,
        slug: page.slug
      }

      pagesMod.push(obj);
    });

    this.setState({
      categories: categoriesMod,
      pages: pagesMod
    });
  }

  render(){
    const categories = this.state.categories.map(categorie => {
      return( <Link key={ categorie.id } to={ `/categorie/${categorie.id}/${categorie.name}` } className="nav-link">{ categorie.name }</Link> )
    });
    const pages = this.state.pages.map(page => { 
      return( <Link key={ page.id } to={ `/page/${page.id}/` } className="nav-link">{ page.name }</Link>) 
    });

    return(
      <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
        <div className="container-fluid">
          <Link to="/" className="navbar-brand">Progetto finale</Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item">
                <Link to="/" className="nav-link">Home</Link>
              </li>
              { pages }
              { categories }
            </ul>
          </div>
        </div>
      </nav>
    );
  }
}

export default Navbar;