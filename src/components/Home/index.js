import React from "react";
import newPost from "../../models/PostModel";
import SinglePost from "../SinglePost";

class Home extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      article: [],
    }
  }

  async componentDidMount(){
    const res = await fetch('http://progetto-finale.test/web/wp-json/wp/v2/posts');
    const posts = await res.json();

    const modPosts = await posts.map( post => newPost(post) );

    this.setState({
      article : modPosts
    });
  }

  render(){
     const post = this.state.article.map(post => <SinglePost key={ post.id } post={ post } />);

    return (
      <div className="m-5">
        <h1>
          Home Page
        </h1>
        <div className='cantainer row justify-content-center g-0'>
          { post }
        </div>
      </div>  
    );
  }
}

export default Home;