import React from "react";
import { withRouter } from "react-router";

class DinamicPage extends React.Component{
  constructor(props){
    super(props);

    this.state = {
      page:{
        title: '',
        content: ''
      }
    }
  }

  async componentDidMount(){
    const res = await fetch(`http://progetto-finale.test/web/wp-json/wp/v2/pages/${this.props.match.params.id}`);
    const page = await res.json();

    const page1 = {
      title: page.title.rendered,
      content: page.content.rendered,
    }

    this.setState({
      page: page1
    });
  }

  render(){
    return(
      <div className="container m-5">
        <h1>{ this.state.page.title}</h1>
        <div dangerouslySetInnerHTML={{ __html: this.state.page.content }}></div>
      </div>     
    );
  }
}

export default withRouter(DinamicPage);