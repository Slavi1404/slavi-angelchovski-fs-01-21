class PostFullPageModel {
  constructor (id, titolo,testo ,imgUrl){
    this.id = id;
    this.title = titolo;
    this.content = testo;
    this.img = imgUrl
  }
}
    
function newPostFullPage(post) {
  return new PostFullPageModel(post.id, post.title.rendered, post.content.rendered, post.better_featured_image.source_url);
}
    
export default newPostFullPage;