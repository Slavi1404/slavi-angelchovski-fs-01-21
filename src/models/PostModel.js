class PostModel {
  constructor (id, titolo, testo ,imgUrl){
    this.id = id;
    this.title = titolo;
    this.excerpt = testo;
    this.img = imgUrl;
  }
}
    
function newPost(post) {
  return new PostModel(post.id, post.title.rendered, post.excerpt.rendered, post.better_featured_image.source_url);
}
    
export default newPost;